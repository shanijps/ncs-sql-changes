1. Set up a new partner specific schema for mapping.
2. Run NCS_table_changes.sql if not done.
3. Run the 'partner_mapper_utility_dump.sql'. This will create a new schema 'partner_mapper_util' with supporting tables and procedure for mapping.
4. Connect this schema to neon-ui application. Run the UI. From the UI --> login page --> signup, create a new partner.
   Check the partner_id (from partner table) of the newly created partner. This id will be needed on the upcoming steps.
5. Open the partner_mapper_util.partner_mapper procedure and run it by passing the new schema name as first parameter , new partner_id from step-4 as the second parameter and partner specific web group id as third parameter, new tenant web user ID as fourth parameter, new tenant account ID as fifth parameter.
   eg :  call partner_mapper_util.partner_mapper('neon','27','1','52','1146460724');
6. Done, See if there are any error logs in partner_mapper_util.mapping_logs