ALTER TABLE `app_attribute_category`
	ADD COLUMN `is_main_category` TINYINT(2) NULL DEFAULT '0' AFTER `base_type`;

UPDATE `app_attribute_category` SET `is_main_category`='1' WHERE  `id`=15;
UPDATE `app_attribute_category` SET `is_main_category`='1' WHERE  `id`=11;
UPDATE `app_attribute_category` SET `is_main_category`='1' WHERE  `id`=8;
UPDATE `app_attribute_category` SET `is_main_category`='1' WHERE  `id`=13;


CREATE TABLE IF NOT EXISTS `internet_configuration` (
  `INTERNET_CONFIGURATION_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PARTNER_ID` bigint(20) DEFAULT NULL,
  `NAME_TAG` varchar(60) NOT NULL,
  `ACCOUNT_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`INTERNET_CONFIGURATION_ID`),
  KEY `INTERT_CONFIG_ACCOUNT_ID_FK_idx` (`PARTNER_ID`),
  CONSTRAINT `INTERT_CONFIG_ACCOUNT_ID_FK` FOREIGN KEY (`PARTNER_ID`) REFERENCES `partner` (`PARTNER_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE IF NOT EXISTS `internet_customer_ip` (
  `INTERNET_CUSTOMER_IP_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `INTERNET_CONFIGURATION_ID` bigint(20) NOT NULL,
  `IP` varchar(60) NOT NULL,
  PRIMARY KEY (`INTERNET_CUSTOMER_IP_ID`),
  KEY `INTERNET_CONFIGURATION_ID_FK_idx` (`INTERNET_CONFIGURATION_ID`),
  CONSTRAINT `INTERNET_CONFIGURATION_ID_FK` FOREIGN KEY (`INTERNET_CONFIGURATION_ID`) REFERENCES `internet_configuration` (`INTERNET_CONFIGURATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE IF NOT EXISTS `static_vpn_download_config` (
  `VPN_CONFIG_ID` int(30) NOT NULL,
  `VPN_CONFIG` longtext NOT NULL,
  PRIMARY KEY (`VPN_CONFIG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO `static_vpn_download_config` (`VPN_CONFIG_ID`, `VPN_CONFIG`) VALUES
	(1, '{"vendorConfigs":[{"platforms":[{"platform":"Gaia","softwareVersions":[{"filename":"customer-gateway-checkpoint.xslt","supportedTypes":["BGP","Static"],"version":"R77.10+"},{"filename":"customer-gateway-checkpoint.xslt","supportedTypes":["BGP","Static"],"version":"R80.10+"}]}],"vendor":"Checkpoint"},{"platforms":[{"platform":"MX Series","softwareVersions":[{"filename":"customer-gateway-cisco-meraki.xslt","supportedTypes":["Static"],"version":"9.0+ (WebUI)"}]}],"vendor":"Cisco Meraki"},{"platforms":[{"platform":"ASA 5500 Series","softwareVersions":[{"filename":"customer-gateway-cisco-asa.xslt","supportedTypes":["Static"],"version":"ASA 8.2+"},{"filename":"customer-gateway-cisco-asa-9.7.xslt","supportedTypes":["BGP","Static"],"version":"ASA 9.7+ VTI"},{"filename":"customer-gateway-cisco-asa-9.xslt","supportedTypes":["Static"],"version":"ASA 9.x"}]},{"platform":"Cisco ASR 1000","softwareVersions":[{"filename":"customer-gateway-cisco-ios-isr.xslt","supportedTypes":["BGP","Static"],"version":"IOS 12.4+"}]},{"platform":"CSRv AMI","softwareVersions":[{"filename":"customer-gateway-cisco-ios-csrv.xslt","supportedTypes":["BGP","Static"],"version":"IOS 12.4+"}]},{"platform":"Firepower","softwareVersions":[{"filename":"customer-gateway-cisco-firepower.xslt","supportedTypes":["Static"],"version":"v6.2.2+"}]},{"platform":"ISR Series Routers","softwareVersions":[{"filename":"customer-gateway-cisco-ios-isr.xslt","supportedTypes":["BGP","Static"],"version":"IOS 12.4+"}]}],"vendor":"Cisco Systems, Inc."},{"platforms":[{"platform":"Netscaler CloudBridge","softwareVersions":[{"filename":"customer-gateway-citrix-ns-cloudbridge.xslt","supportedTypes":["Static"],"version":"NS 11+"}]}],"vendor":"Citrix"},{"platforms":[{"platform":"CR15iNG","softwareVersions":[{"filename":"customer-gateway-cyberoam-config.xslt","supportedTypes":["Static"],"version":"V 10.6.5 MR-1"}]}],"vendor":"Cyberoam"},{"platforms":[{"platform":"NSA, TZ","softwareVersions":[{"filename":"customer-gateway-sonic-wall-5.8.xslt","supportedTypes":["Static"],"version":"OS 5.8"},{"filename":"customer-gateway-sonic-wall.xslt","supportedTypes":["BGP","Static"],"version":"OS 5.9"},{"filename":"customer-gateway-sonic-wall.xslt","supportedTypes":["BGP","Static"],"version":"OS 6.2"}]}],"vendor":"Dell SonicWall"},{"platforms":[{"platform":"BIG-IP","softwareVersions":[{"filename":"customer-gateway-bigip-f5.xslt","supportedTypes":["BGP","Static"],"version":"v12.0.0+"}]}],"vendor":"F5 Networks, Inc."},{"platforms":[{"platform":"Fortigate 40+ Series","softwareVersions":[{"filename":"customer-gateway-fortigate.xslt","supportedTypes":["BGP","Static"],"version":"FortiOS 4.0+"},{"filename":"customer-gateway-fortigate-gui.xslt","supportedTypes":["BGP","Static"],"version":"FortiOS 4.0+ (GUI)"},{"filename":"customer-gateway-fortigate-v5.0.xslt","supportedTypes":["BGP","Static"],"version":"FortiOS 5.0+"},{"filename":"customer-gateway-fortigate-gui-v5.0.xslt","supportedTypes":["BGP","Static"],"version":"FortiOS 5.0+ (GUI)"}]}],"vendor":"Fortinet"},{"platforms":[{"platform":"Generic","softwareVersions":[{"filename":"customer-gateway-generic.xslt","supportedTypes":["BGP","Static"],"version":"Vendor Agnostic"}]}],"vendor":"Generic"},{"platforms":[{"platform":"MSR800","softwareVersions":[{"filename":"customer-gateway-h3c.xslt","supportedTypes":["BGP","Static"],"version":"Version 5.20"}]}],"vendor":"H3C"},{"platforms":[{"platform":"SEIL/B1","softwareVersions":[{"filename":"customer-gateway-iij-seil.xslt","supportedTypes":["BGP","Static"],"version":"SEIL/B1 3.70+"}]},{"platform":"SEIL/X1 and SEIL/X2","softwareVersions":[{"filename":"customer-gateway-iij-seil.xslt","supportedTypes":["BGP","Static"],"version":"SEIL/X 3.70+"}]},{"platform":"SEIL/x86","softwareVersions":[{"filename":"customer-gateway-iij-seil.xslt","supportedTypes":["BGP","Static"],"version":"SEIL/x86 2.30+"}]}],"vendor":"IIJ"},{"platforms":[{"platform":"J-Series Routers","softwareVersions":[{"filename":"customer-gateway-juniper-junos-j.xslt","supportedTypes":["BGP","Static"],"version":"JunOS 9.5+"}]},{"platform":"SRX Routers","softwareVersions":[{"filename":"customer-gateway-juniper-srx.xslt","supportedTypes":["BGP","Static"],"version":"JunOS 11.0+"}]},{"platform":"SSG and ISG Series Routers","softwareVersions":[{"filename":"customer-gateway-juniper-screenos-6.1.xslt","supportedTypes":["BGP","Static"],"version":"ScreenOS 6.1"},{"filename":"customer-gateway-juniper-screenos-6.2.xslt","supportedTypes":["BGP","Static"],"version":"ScreenOS 6.2+"},{"filename":"customer-gateway-juniper-screenos-gui-6.2.xslt","supportedTypes":["BGP","Static"],"version":"ScreenOS 6.2+ (GUI)"}]}],"vendor":"Juniper Networks, Inc."},{"platforms":[{"platform":"Windows Server","softwareVersions":[{"filename":"customer-gateway-windows-server-2008.xslt","supportedTypes":["Static"],"version":"2008 R2"},{"filename":"customer-gateway-windows-server-2012.xslt","supportedTypes":["Static"],"version":"2012 R2"}]}],"vendor":"Microsoft"},{"platforms":[{"platform":"RouterOS","softwareVersions":[{"filename":"customer-gateway-mikrotik-routeros-v6.36.xslt","supportedTypes":["BGP","Static"],"version":"6.36"}]}],"vendor":"Mikrotik"},{"platforms":[{"platform":"Openswan","softwareVersions":[{"filename":"customer-gateway-openswan.xslt","supportedTypes":["Static"],"version":"Openswan 2.6.38+"}]}],"vendor":"Openswan"},{"platforms":[{"platform":"PA Series","softwareVersions":[{"filename":"customer-gateway-paloalto.xslt","supportedTypes":["BGP","Static"],"version":"PANOS 4.1.2+"},{"filename":"customer-gateway-paloalto-gui.xslt","supportedTypes":["BGP","Static"],"version":"PANOS 4.1.2+ (GUI)"},{"filename":"customer-gateway-paloalto-7-0.xslt","supportedTypes":["BGP","Static"],"version":"PANOS 7.0+"}]}],"vendor":"Palo Alto Networks"},{"platforms":[{"platform":"pfSense","softwareVersions":[{"filename":"customer-gateway-pfsense.xslt","supportedTypes":["Static"],"version":"pfsense 2.2.5+(GUI)"}]}],"vendor":"pfSense"},{"platforms":[{"platform":"Ubuntu 16.04","softwareVersions":[{"filename":"customer-gateway-strongswan.xslt","supportedTypes":["Static"],"version":"Strongswan 5.5.1+"}]}],"vendor":"Strongswan"},{"platforms":[{"platform":"XTM, Firebox","softwareVersions":[{"filename":"customer-gateway-watchguard.xslt","supportedTypes":["Static"],"version":"Fireware OS 11.11.4 (WebUI)"},{"filename":"customer-gateway-watchguardnew.xslt","supportedTypes":["BGP","Static"],"version":"Fireware OS 11.12.2+ (WebUI)"}]}],"vendor":"WatchGuard, Inc."},{"platforms":[{"platform":"RTX Routers","softwareVersions":[{"filename":"customer-gateway-yamaha-rtx.xslt","supportedTypes":["BGP","Static"],"version":"Rev.10.01.16+"}]}],"vendor":"Yamaha"},{"platforms":[{"platform":"Zywall Series","softwareVersions":[{"filename":"customer-gateway-zyxel420.xslt","supportedTypes":["Static"],"version":"Zywall 4.20+"}]}],"vendor":"Zyxel"}]}');


CREATE TABLE IF NOT EXISTS `vpn_configuration` (
  `VPN_CONFIGURATION_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PARTNER_ID` bigint(20) DEFAULT NULL,
  `ACCOUNT_ID` bigint(20) NOT NULL,
  `NAME_TAG` varchar(60) NOT NULL,
  `CUSTOMER_GATEWAY_IP` varchar(45) NOT NULL,
  `FTP_IP` varchar(45) DEFAULT NULL,
  `SMSC_IP` varchar(45) DEFAULT NULL,
  `IN_IP` varchar(45) DEFAULT NULL,
  `VENDOR` varchar(60) NOT NULL,
  `PLATFORM` varchar(60) NOT NULL,
  `SOFTWARE` varchar(60) NOT NULL,
  `CUSTOMER_GATEWAY_ID` longtext,
  `VPN_CONNECTION_ID` longtext,
  `IS_DOWNLOAD_AVAILABLE` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`VPN_CONFIGURATION_ID`),
  KEY `vpn_config_partner_id_fk` (`PARTNER_ID`),
  CONSTRAINT `vpn_config_partner_id_fk` FOREIGN KEY (`PARTNER_ID`) REFERENCES `partner` (`PARTNER_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `vpn_whitelist_ip` (
  `VPN_WHITELIST_IP_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VPN_CONFIGURATION_ID` bigint(20) NOT NULL,
  `IP` varchar(60) NOT NULL,
  PRIMARY KEY (`VPN_WHITELIST_IP_ID`),
  KEY `VPN_CONFIGURATION_ID_FK_idx` (`VPN_CONFIGURATION_ID`),
  CONSTRAINT `VPN_CONFIGURATION_ID_FK` FOREIGN KEY (`VPN_CONFIGURATION_ID`) REFERENCES `vpn_configuration` (`VPN_CONFIGURATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE IF NOT EXISTS `customer_wizard_progress_tracker` (
  `CUSTOMER_WIZARD_PROGRESS_TRACKER_ID` int(60) NOT NULL AUTO_INCREMENT,
  `PARTNER_ID` bigint(20) DEFAULT NULL,
  `ACCOUNT_ID` bigint(20) NOT NULL,
  `IS_CUSTOMER_DATA_CONFIGURED` tinyint(4) DEFAULT '0',
  `IS_MARKETING_CONFIGURED` tinyint(4) DEFAULT '0',
  `IS_INBOUND_CONFIGURED` tinyint(4) DEFAULT '0',
  `IS_OUTBOUND_CONFIGURED` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`CUSTOMER_WIZARD_PROGRESS_TRACKER_ID`),
  KEY `WIZARD_PARTNER_ID_FK_idx` (`PARTNER_ID`),
  CONSTRAINT `WIZARD_PROGRESS_PARTNER_ID_FK` FOREIGN KEY (`PARTNER_ID`) REFERENCES `partner` (`PARTNER_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


INSERT INTO `web_role` (`WEB_ROLE_ID`, `ROLE_NAME`, `DESCRIPTION`, `HEADING_ID`, `REQUIRED`) VALUES ('20000', 'netAdminPck', 'Network Administration Package', '27', '0');
