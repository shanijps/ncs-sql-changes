CREATE DATABASE  IF NOT EXISTS `partner_mapper_util` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `partner_mapper_util`;
-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 192.168.150.143    Database: partner_mapper_util
-- ------------------------------------------------------
-- Server version	5.7.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `invalid_tables`
--

DROP TABLE IF EXISTS `invalid_tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `invalid_tables` (
  `table_name` varchar(255) NOT NULL,
  PRIMARY KEY (`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AVG_ROW_LENGTH=230;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invalid_tables`
--

LOCK TABLES `invalid_tables` WRITE;
/*!40000 ALTER TABLE `invalid_tables` DISABLE KEYS */;
INSERT INTO `invalid_tables` VALUES ('app_approval_rule'),('ffe_response_code_group'),('partner_mobile_address'),('tenant_mapping'),('web_user_password_policy');
/*!40000 ALTER TABLE `invalid_tables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mapping_logs`
--

DROP TABLE IF EXISTS `mapping_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mapping_logs` (
  `mapping_log_id` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `schema_name` varchar(64) DEFAULT NULL,
  `table_name` varchar(64) DEFAULT NULL,
  `message` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mapping_logs`
--

LOCK TABLES `mapping_logs` WRITE;
/*!40000 ALTER TABLE `mapping_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `mapping_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'partner_mapper_util'
--

--
-- Dumping routines for database 'partner_mapper_util'
--
/*!50003 DROP PROCEDURE IF EXISTS `mapping_logger` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`neon`@`%` PROCEDURE `mapping_logger`(IN schema_name varchar(255), IN table_name varchar(255), IN message text)
BEGIN

  SET @qry = CONCAT("INSERT INTO partner_mapper_util.mapping_logs values (now(),'", schema_name, "','", table_name, "',", QUOTE(message), ")");
  PREPARE statement FROM @qry;
  EXECUTE statement;
  DEALLOCATE PREPARE statement;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `partner_mapper` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`neon`@`%` PROCEDURE `partner_mapper`(IN schemaName varchar(255), IN newPartnerId varchar(255), IN tenantWebgroupId varchar(255), IN tenantWebUserId varchar(255), IN tenantAccountId varchar(255))
BEGIN

  DECLARE tableName varchar(100) DEFAULT "";
  DECLARE finished tinyint DEFAULT 0;
  DECLARE moreRowException tinyint DEFAULT 0;
  DECLARE tenantPartnerId varchar(100) DEFAULT "";

  DECLARE allTables CURSOR FOR
  #select all tables having coloumn name partner_id and having rows count >=1
  SELECT DISTINCT
    TABLE_NAME
  FROM INFORMATION_SCHEMA.COLUMNS
  WHERE COLUMN_NAME = 'PARTNER_ID'
  AND TABLE_SCHEMA = schemaName
  AND table_name IN (SELECT
      table_name
    FROM information_schema.tables
    WHERE table_rows >= 1
    AND TABLE_SCHEMA = schemaName);

  #cursor for all tenent partner ids
  DECLARE alltenantPartners CURSOR FOR
  SELECT
    PARTNER_ID
  FROM partner_mapper_util.partner_id_vw;


  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE,
    @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
    SET @full_error = CONCAT("ERROR ", @errno, " (", @sqlstate, "): ", @text);
    ROLLBACK;
    SET autocommit = 1;
    #all the logs will be avaialble in mapping_logs table
    CALL mapping_logger(schemaName, tableName, @full_error);
  END;

  DECLARE CONTINUE HANDLER
  FOR NOT FOUND SET finished = 1;

  OPEN allTables;
  SET autocommit = 0;
  SET SQL_SAFE_UPDATES = 0;

updatePartner:
  LOOP FETCH allTables INTO tableName;

    IF finished = 1 THEN
      LEAVE updatePartner;
    END IF;

    SET @isValidTable = 0;

    SET @qry = CONCAT("SELECT COUNT(*) into @isValidTable
FROM partner_mapper_util.invalid_tables  
WHERE table_name='", tableName, "'");
    PREPARE statement FROM @qry;
    EXECUTE statement;
    DEALLOCATE PREPARE statement;

    #skip if table is not avaiable in invalid table list
    IF @isValidTable = 1 THEN
      CALL mapping_logger(schemaName, tableName, 'Invalid table');
      ITERATE updatePartner;
    END IF;


    IF tableName = 'web_user_sudo_partner'
      OR tableName = 'web_user' THEN
      #step:1 update all the parent ids in partner table and exclude the new partner id from any updation
      SET @qry = CONCAT("UPDATE ", schemaName, ".", tableName, ' SET PARTNER_ID = ', newPartnerId, ' WHERE PARTNER_ID = 1 AND WEB_USER_ID != 1 ');
      PREPARE statement FROM @qry;
      EXECUTE statement;
      DEALLOCATE PREPARE statement;

      #map all the web users to new tenant web users so that the user will get all tenant mapping as that of system global
      IF tableName = 'web_user_sudo_partner' THEN
        SET @qry = CONCAT("UPDATE ", schemaName, ".", tableName, ' SET WEB_USER_ID = ', tenantWebUserId, ' WHERE WEB_USER_ID = 1 AND PARTNER_ID NOT IN (1,', newPartnerId, ')');
        PREPARE statement FROM @qry;
        EXECUTE statement;
        DEALLOCATE PREPARE statement;
      END IF;
     
      #set touch point id to null for the flyops web user so that the reference table im_touchpoint doesnt have invalid reference.
      IF tableName = 'web_user' THEN
        SET @qry = CONCAT("UPDATE ", schemaName, ".", tableName, ' SET touchpoint_id = null WHERE WEB_USER_ID = 1 ');
        PREPARE statement FROM @qry;
        EXECUTE statement;
        DEALLOCATE PREPARE statement;
      END IF;
     
      CALL mapping_logger(schemaName, tableName, 'Updated');
      ITERATE updatePartner;
    END IF;

    IF tableName = 'partner' THEN
      #step:1 update all the parent ids in partner table and exclude the new  partner id from any updation
      SET @qry = CONCAT("UPDATE ", schemaName, ".", tableName, ' SET PARENT_ID = ', newPartnerId, ' WHERE PARENT_ID = 1 AND PARTNER_ID != ', newPartnerId);
      PREPARE statement FROM @qry;
      EXECUTE statement;
      DEALLOCATE PREPARE statement;
      CALL mapping_logger(schemaName, tableName, 'Updated');
      ITERATE updatePartner;
    END IF;
    
	IF tableName = 'im_additional_params' THEN
      SET @qry = CONCAT("DELETE FROM ", schemaName, ".", tableName, ' WHERE PARTNER_ID = ', newPartnerId);
      PREPARE statement FROM @qry;
      EXECUTE statement;
      DEALLOCATE PREPARE statement;
      SET @qry = CONCAT("UPDATE ", schemaName, ".", tableName, ' SET PARTNER_ID = ', newPartnerId, " WHERE PARTNER_ID = 1");
      PREPARE statement FROM @qry;
      EXECUTE statement;
      DEALLOCATE PREPARE statement;
      CALL mapping_logger(schemaName, tableName, 'Updated');
      ITERATE updatePartner;
    END IF;
    
	IF tableName = 'ext_track_source' THEN
      SET @qry = CONCAT("UPDATE ", schemaName, ".", tableName, ' SET PARTNER_ID = ', newPartnerId, " WHERE source_id NOT IN (1,2,3,4,5) AND PARTNER_ID=1");
      PREPARE statement FROM @qry;
      EXECUTE statement;
      DEALLOCATE PREPARE statement;
      CALL mapping_logger(schemaName, tableName, 'Updated');
      ITERATE updatePartner;
    END IF;
    
    IF tableName = 'partner_domain_field' THEN
      #step:1 update all the parent ids in partner table and exclude the new  partner id from any updation
      SET @qry = CONCAT("DELETE FROM ", schemaName, ".", tableName, ' WHERE PARTNER_ID = ', newPartnerId);
      PREPARE statement FROM @qry;
      EXECUTE statement;
      DEALLOCATE PREPARE statement;

      SET @qry = CONCAT("UPDATE ", schemaName, ".", tableName, ' SET PARTNER_ID = ', newPartnerId, " WHERE PARTNER_ID = 1 ");
      PREPARE statement FROM @qry;
      EXECUTE statement;
      DEALLOCATE PREPARE statement;

      CALL mapping_logger(schemaName, tableName, 'Updated');
      ITERATE updatePartner;
    END IF;
    
    #default black out rule is already created for the new tenant in signup page, hence the SG default blackout rule is removing
    IF tableName = 'partner_blackout_rule' THEN
      #first delete the child reference in table partner_inventory_definition for partner_blackout_rule created by new partner in signup page.
	  SET @qry = CONCAT("DELETE FROM ", schemaName, ".",'partner_inventory_definition WHERE PARTNER_ID = ',newPartnerId);
      PREPARE statement FROM @qry;
      EXECUTE statement;
      DEALLOCATE PREPARE statement;
      SET @qry = CONCAT("DELETE FROM ", schemaName, ".", tableName, ' WHERE PARTNER_ID = ',newPartnerId,' AND IS_DEFAULT = 1');
      PREPARE statement FROM @qry;
      EXECUTE statement;
      DEALLOCATE PREPARE statement;
    END IF;
    
    #created by and updated by is considering in segment analysis UI,hence table needs special changes
    IF tableName = 'report_insight_analysis_job' OR
       tableName = 'insight_analysis_template' THEN
	  SET @qry = CONCAT("UPDATE ", schemaName, ".", tableName, ' SET CREATED_BY = ', tenantWebUserId, ' WHERE CREATED_BY = 1 ');
      PREPARE statement FROM @qry;
      EXECUTE statement;
      DEALLOCATE PREPARE statement;
	  SET @qry = CONCAT("UPDATE ", schemaName, ".", tableName, ' SET UPDATED_BY = ', tenantWebUserId, ' WHERE UPDATED_BY = 1 ');
      PREPARE statement FROM @qry;
      EXECUTE statement;
      DEALLOCATE PREPARE statement;
    END IF;
    
    # at the end of this loop, change all the system global partner_id (1) with new  partner id
    SET @qry = CONCAT("UPDATE ", schemaName, ".", tableName, ' SET PARTNER_ID = ', newPartnerId, ' WHERE PARTNER_ID = 1 ');
    PREPARE statement FROM @qry;
    EXECUTE statement;
    DEALLOCATE PREPARE statement;
    CALL mapping_logger(schemaName, tableName, 'Updated');

  END LOOP updatePartner;

  # in oz_entity table, external_reference_id can be a partner_id when the entity_type_id=1. Hence this table needs a special update.
  SET @qry = CONCAT("UPDATE ", schemaName, ".", 'oz_entity SET EXTERNAL_REFERENCE_ID = ', newPartnerId, " WHERE EXTERNAL_REFERENCE_ID = 1 AND ENTITY_TYPE_ID=1");
  PREPARE statement FROM @qry;
  EXECUTE statement;
  DEALLOCATE PREPARE statement;

  SET @newPartnerNameForProfile = '';
  
  SET @qry = CONCAT("SELECT PARTNER_NAME into @newPartnerNameForProfile FROM ",schemaName, ".","partner WHERE partner_id = ",newPartnerId);
  PREPARE statement FROM @qry;
  EXECUTE statement;
  DEALLOCATE PREPARE statement;
  
  SET @newPartnerNameForProfile = CONCAT('Profile (',@newPartnerNameForProfile,')');
  
  SET @qry = CONCAT('UPDATE ', schemaName, '.', "oz_entity SET ENTITY_NAME = ",'\'',@newPartnerNameForProfile,'\'',' WHERE EXTERNAL_REFERENCE_ID = ',newPartnerId,' AND ENTITY_TYPE_ID=1');
  PREPARE statement FROM @qry;
  EXECUTE statement;
  DEALLOCATE PREPARE statement;
  
  CALL mapping_logger(schemaName, 'oz_entity', 'Updated');

  SET tableName = 'web_group_user';
  #removing all the web groups from all the web users so that the current web users will get new role(the new role is already created during signup)
  SET @qry = CONCAT("DELETE wgu1.* FROM ", schemaName, '.web_group_user wgu1 INNER JOIN ', schemaName, '.web_group_user wgu2 WHERE wgu1.WEB_USER_ID = wgu2.WEB_USER_ID and  wgu1.WEB_USER_ID!=1 and  wgu2.WEB_USER_ID!=1 AND wgu1.WEB_GROUP_ID > wgu2.WEB_GROUP_ID');
  PREPARE statement FROM @qry;
  EXECUTE statement;
  DEALLOCATE PREPARE statement;

  #assigning the new role id to all current web users except system global user (1)
  SET @qry = CONCAT("UPDATE ", schemaName, ".web_group_user SET WEB_GROUP_ID = '", tenantWebgroupId, "'  WHERE WEB_USER_ID != 1");
  PREPARE statement FROM @qry;
  EXECUTE statement;
  DEALLOCATE PREPARE statement;
  CALL mapping_logger(schemaName, tableName, 'Updated');

  CLOSE allTables;


  #updating partner_inventory_definition table in column 'created_partner_id'
  SET @qry = CONCAT("UPDATE ", schemaName, ".partner_inventory_definition SET CREATED_PARTNER_ID = '", newPartnerId, "'  WHERE CREATED_PARTNER_ID = '1' AND PARTNER_ID = '", newPartnerId, '\'');
  PREPARE statement FROM @qry;
  EXECUTE statement;
  DEALLOCATE PREPARE statement;
  CALL mapping_logger(schemaName, 'partner_inventory_definition', 'Updated');


  #updating partner_inventory_deligation table in column 'created_partner_id'
  SET @qry = CONCAT("UPDATE ", schemaName, ".partner_inventory_deligation SET CREATED_PARTNER_ID = '", newPartnerId, "'  WHERE CREATED_PARTNER_ID = '1' AND PARTNER_ID = '", newPartnerId, '\'');
  PREPARE statement FROM @qry;
  EXECUTE statement;
  DEALLOCATE PREPARE statement;
  CALL mapping_logger(schemaName, 'partner_inventory_deligation', 'Updated');


  #updating partner_metric_share table in column 'SHARED_PARTNER_ID'
  SET @qry = CONCAT("UPDATE ", schemaName, ".partner_metric_share SET SHARED_PARTNER_ID = '", newPartnerId, "'  WHERE SHARED_PARTNER_ID = '1' ");
  PREPARE statement FROM @qry;
  EXECUTE statement;
  DEALLOCATE PREPARE statement;
  CALL mapping_logger(schemaName, 'partner_metric_share', 'Updated');


  #updating partner_partner_event_share table in column 'SHARED_PARTNER_ID'
  SET @qry = CONCAT("UPDATE ", schemaName, ".partner_partner_event_share SET SHARED_PARTNER_ID = '", newPartnerId, "'  WHERE SHARED_PARTNER_ID = '1' ");
  PREPARE statement FROM @qry;
  EXECUTE statement;
  DEALLOCATE PREPARE statement;
  CALL mapping_logger(schemaName, 'partner_partner_event_share', 'Updated');


  #updating partner_reg_list_share table in column 'SHARED_PARTNER_ID'
  SET @qry = CONCAT("UPDATE ", schemaName, ".partner_reg_list_share SET SHARED_PARTNER_ID = '", newPartnerId, "'  WHERE SHARED_PARTNER_ID = '1' ");
  PREPARE statement FROM @qry;
  EXECUTE statement;
  DEALLOCATE PREPARE statement;
  CALL mapping_logger(schemaName, 'partner_reg_list_share', 'Updated');


  #drop partner view if already exists
  SET @qry = CONCAT("DROP VIEW IF EXISTS partner_mapper_util.partner_id_vw");
  PREPARE statement FROM @qry;
  EXECUTE statement;
  DEALLOCATE PREPARE statement;

  #create a temporary view for partner id mapping
  SET @qry = CONCAT("create view partner_mapper_util.partner_id_vw as SELECT PARTNER_ID FROM ", schemaName, ".partner p WHERE p.PARTNER_ID!=1");
  PREPARE statement FROM @qry;
  EXECUTE statement;
  DEALLOCATE PREPARE statement;

  #removing all rows from the tenant_mapping and keep only the 1111111111-1 mapping
  SET @qry = CONCAT("DELETE FROM ", schemaName, ".tenant_mapping where  ACCOUNT_ID !='1111111111'");
  PREPARE statement FROM @qry;
  EXECUTE statement;
  DEALLOCATE PREPARE statement;

  OPEN alltenantPartners;
  SET finished = 0;
updateTenantMapping:
  LOOP FETCH alltenantPartners INTO tenantPartnerId;
    IF finished = 1 THEN
      LEAVE updateTenantMapping;
    END IF;

    #map the new tenenat partner id and account id
    SET @qry = CONCAT("INSERT INTO ", schemaName, ".tenant_mapping values ('", tenantAccountId, '\',\'', tenantPartnerId, '\')');
    PREPARE statement FROM @qry;
    EXECUTE statement;
    DEALLOCATE PREPARE statement;

  END LOOP updateTenantMapping;
  CLOSE alltenantPartners;

  #droping the view after using
  SET @qry = CONCAT("DROP VIEW IF EXISTS partner_mapper_util.partner_id_vw");
  PREPARE statement FROM @qry;
  EXECUTE statement;
  DEALLOCATE PREPARE statement;


  CALL mapping_logger(schemaName, 'tenant_mapping', 'Updated');
  CALL mapping_logger(schemaName, '', 'Successfully mapped new partner');

  COMMIT;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-19 12:05:58
