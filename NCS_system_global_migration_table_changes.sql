#changes from Shanij satrts here
CREATE TABLE IF NOT EXISTS `tenant_mapping` (
  `ACCOUNT_ID` bigint(20) NOT NULL,
  `PARTNER_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`ACCOUNT_ID`,`PARTNER_ID`),
  UNIQUE KEY `TENANT_MAPPING_PK` (`ACCOUNT_ID`,`PARTNER_ID`),
  UNIQUE KEY `TENANT_MAPPING_UK2` (`ACCOUNT_ID`,`PARTNER_ID`),
  KEY `TENANT_MAPPING_PARTNER_ID_FK` (`PARTNER_ID`),
  CONSTRAINT `TENANT_MAPPING_PARTNER_ID_FK` FOREIGN KEY (`PARTNER_ID`) REFERENCES `partner` (`PARTNER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `tenant_mapping` (`ACCOUNT_ID`, `PARTNER_ID`) VALUES ('1111111111', '1');

INSERT INTO `email_template` (`key_name`, `template`) VALUES ('template.email.tenant.registration.en', '<html><head> <title></title></head><body> <table width="100%" cellpadding="0" cellspacing="0" border="0"> <tr> <td> <a href="http://$siteName" id="logo" title="Home"><img src="cid:neonLogo" style="vertical-align: middle" title="" alt="Neon" border="0" /></a> </td> </tr> <tr> <td style="background-color:#4781D6; margin:0;padding:0;height:3px;" /></td> </tr> <tr> <td>&nbsp;</td> </tr> <tr> <td> <span style="font-size: 13.5pt; font-family: Arial;">Dear $firstname,</span> </td> </tr> <tr> <td>&nbsp;</td> </tr> <tr> <td><span style="font-size: 10pt; color: #666666; font-family: Arial;">Your new NEON Cloud Service account has been created.</span></td> </tr> <tr> <td><span style="font-size: 10pt; color: #666666; font-family: Arial;">You have been assigned a temporary password. You should change this the next time you log into NEON.</span></td> </tr> <tr> <td>&nbsp;</td> </tr> <tr> <td> <table style="width: 400px; border-collapse: collapse; height: 47px;" cellspacing="0" cellpadding="2" width="499" border="0"> <tr> <td valign="top" width="150"> <span style="font-weight: bold;font-size: 10pt;font-family: Arial;">Username:</span></td> <td valign="top" width="250"> <span style="font-size: 10pt;font-family: Arial;">$username</span></td> </tr> <tr> <td valign="top" width="150"> <span style="font-weight: bold;font-size: 10pt;font-family: Arial;">Temporary Password:</span></td> <td valign="top" width="250"> <span style="font-size: 10pt;font-family: Arial;">$password</span></td> </tr> <tr> <td valign="top" width="150"> <span style="font-weight: bold;font-size: 10pt;font-family: Arial;">Account ID:</span></td> <td valign="top" width="250"> <span style="font-size: 10pt;font-family: Arial;">$accountId</span></td> </tr> </table> </td> </tr> <tr> <td>&nbsp;</td> </tr> <tr> <td style="background-color:#4781D6; margin:0;padding:0;height:3px;" /></td> </tr> <tr> <td>&nbsp;</td> </tr> <tr> <td> <span style="font-size: 10pt; font-family: Arial;">Please click here to <a title="Log in" href="http://$siteName">Log in</a> to NCS<br/> </span> </td> </tr> <tr> <td>&nbsp;</td> </tr> <tr> <td> <span style="font-size: 8pt; color: #cccccc; font-family: Arial;"> Please do not reply to this message, the information in this email is system generated. </span> </td> </tr> </table></body></html>"');

INSERT INTO `email_template` (`key_name`, `template`) VALUES ('template.email.tenant.registration.es', '<html><head> <title></title></head><body> <table width="100%" cellpadding="0" cellspacing="0" border="0"> <tr> <td> <a href="http://$siteName" id="logo" title="Home"> <img src="cid:neonLogo" style="vertical-align: middle" title="" alt="Neon" border="0" /> </a> </td> </tr> <tr> <td style="background-color:#4781D6; margin:0;padding:0;height:3px;" /></td> </tr> <tr> <td>&nbsp;</td> </tr> <tr> <td> <span style="font-size: 13.5pt; font-family: Arial;">Querido $firstname,</span> </td> </tr> <tr> <td>&nbsp;</td> </tr> <tr> <td><span style="font-size: 10pt; color: #666666; font-family: Arial;">Se ha creado su nueva cuenta de NEON Cloud Service.</span></td> </tr> <tr> <td><span style="font-size: 10pt; color: #666666; font-family: Arial;">Le han asignado una contraseña temporal. Debe cambiar esto la próxima vez que inicie sesión en NEON-dX.</span></td> </tr> <tr> <td>&nbsp;</td> </tr> <tr> <td> <table style="width: 400px; border-collapse: collapse; height: 47px;" cellspacing="0" cellpadding="2" width="499" border="0"> <tr> <td valign="top" width="150"> <span style="font-weight: bold;font-size: 10pt;font-family: Arial;">Nombre de usuario:</span></td> <td valign="top" width="250"> <span style="font-size: 10pt;font-family: Arial;">$username</span></td> </tr> <tr> <td valign="top" width="150"> <span style="font-weight: bold;font-size: 10pt;font-family: Arial;">Temporal Contraseña:</span></td> <td valign="top" width="250"> <span style="font-size: 10pt;font-family: Arial;">$password</span></td> </tr> <tr> <td valign="top" width="150"> <span style="font-weight: bold;font-size: 10pt;font-family: Arial;">Cuenta ID:</span></td> <td valign="top" width="250"> <span style="font-size: 10pt;font-family: Arial;">$accountId</span></td> </tr> </table> </td> </tr> <tr> <td>&nbsp;</td> </tr> <tr> <td style="background-color:#4781D6; margin:0;padding:0;height:3px;" /></td> </tr> <tr> <td>&nbsp;</td> </tr> <tr> <td> <span style="font-size: 10pt; font-family: Arial;">Por favor haga clic aquí para <a title="Log in" href="http://$siteName">Iniciar sesión en</a> NCS<br/> </span> </td> </tr> <tr> <td>&nbsp;</td> </tr> <tr> <td> <span style="font-size: 8pt; color: #cccccc; font-family: Arial;"> No responda a este mensaje, la información en este correo electrónico es generada por el sistema. </span> </td> </tr> </table></body></html>');

ALTER TABLE `export_location_details` 
ADD COLUMN `PARTNER_ID` BIGINT(20) NULL  DEFAULT 1 AFTER `IS_FTP_LOCATION`,
ADD INDEX `EXPORT_LOCATION_PARTNER_ID_FK_idx` (`PARTNER_ID` ASC);

ALTER TABLE `export_location_details` 
ADD CONSTRAINT `EXPORT_LOCATION_PARTNER_ID_FK`
  FOREIGN KEY (`PARTNER_ID`)
  REFERENCES `partner` (`PARTNER_ID`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `ffe_reward_type` 
ADD COLUMN `PARTNER_ID` BIGINT(20) NULL  DEFAULT 1 AFTER `FLOW_CLASS_ID`,
ADD INDEX `FFE_REWARD_TYPE_PARTNER_ID_FK_idx` (`PARTNER_ID` ASC);

ALTER TABLE `ffe_reward_type` 
ADD CONSTRAINT `FFE_REWARD_TYPE_PARTNER_ID_FK`
  FOREIGN KEY (`PARTNER_ID`)
  REFERENCES `partner` (`PARTNER_ID`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;

ALTER TABLE `ffe_flow_class` 
ADD COLUMN `PARTNER_ID` BIGINT(20) NULL  DEFAULT 1 AFTER `FLOW_CLASS_VALUE`,
ADD INDEX `FFE_FLOW_CLASS_PARTNER_ID_FK_idx` (`PARTNER_ID` ASC);

ALTER TABLE `ffe_flow_class` 
ADD CONSTRAINT `FFE_FLOW_CLASS_PARTNER_ID_FK`
  FOREIGN KEY (`PARTNER_ID`)
  REFERENCES `partner` (`PARTNER_ID`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;
  
  
ALTER TABLE `email_header_counter` 
ADD COLUMN `PARTNER_ID` BIGINT(20) NULL  DEFAULT 1 AFTER `USED`,
ADD INDEX `EMAIL_HEADER_COUNTER_PARTNER_ID_FK_idx` (`PARTNER_ID` ASC);

ALTER TABLE `email_header_counter` 
ADD CONSTRAINT `EMAIL_HEADER_COUNTER_PARTNER_ID_FK`
  FOREIGN KEY (`PARTNER_ID`)
  REFERENCES `partner` (`PARTNER_ID`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;
  
UPDATE `export_location_details` SET `PARTNER_ID` = '1' WHERE `PARTNER_ID` IS NULL;
UPDATE `ffe_reward_type` SET `PARTNER_ID` = '1' WHERE `PARTNER_ID` IS NULL;
UPDATE `ffe_flow_class` SET `PARTNER_ID` = '1' WHERE `PARTNER_ID` IS NULL;
UPDATE email_header_counter SET `PARTNER_ID` = '1' WHERE `PARTNER_ID` IS NULL;
#changes from Shanij ends here
  
#changes from Amal starts here
alter table track_event_type drop index event_type_name_uq; 
alter table track_event_type add unique key event_type_name_uq (event_type_name,partner_id);
alter table im_touchpoint drop index im_touchpoint_uq; 
alter table im_touchpoint add unique key im_touchpoint_uq (name,type,partner_id);
#changes from Amal ends here

#changes from Anjali starts here
ALTER TABLE job_dk2
ADD COLUMN `PARTNER_ID` BIGINT(20) NULL  DEFAULT 1 AFTER `UPDATED_BY`,
ADD INDEX `JOB_DK2_PARTNER_ID_FK_idx` (`PARTNER_ID` ASC);

ALTER TABLE job_dk2 
ADD CONSTRAINT `JOB_DK2_PARTNER_ID_FK`
  FOREIGN KEY (`PARTNER_ID`)
  REFERENCES `partner` (`PARTNER_ID`)
  ON DELETE NO ACTION
  ON UPDATE CASCADE;
  
UPDATE job_dk2 SET `PARTNER_ID` = '1' WHERE `PARTNER_ID` IS NULL;
#changes from Anjali ends here

