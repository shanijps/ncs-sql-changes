
INSERT INTO `external_connectors` (`CONNECTOR_ID`, `CONN_NAME`, `CONFIG_HELP`, `FIELD_NAMES`, `IMAGE_URL`, `connector_type`, `translator_class_name`, `display_image_url`, `OUTBOUND_CONNECTOR_TYPE_ID`) VALUES
	(19, 'smpp', NULL, NULL, NULL, 2, NULL, '/images/connectors-logo/tcp.png', 5);
	

==================================================================================================================================

ALTER TABLE `external_connectors` 
ADD COLUMN `OUTBOUND_CONNECTOR_TYPE_ID` BIGINT(20) NULL AFTER `display_image_url`,
CHANGE COLUMN `CONNECTOR_ID` `CONNECTOR_ID` BIGINT(20) NOT NULL ;

============================================================================================

CREATE TABLE IF NOT EXISTS `static_smpp_tlv_tag` (
  `STATIC_SMPP_TLV_TAG_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PARAM_ID` bigint(20) NOT NULL,
  `DISPLAY_NAME` varchar(200) NOT NULL,
  `EXAMPLE_TEXT` varchar(200) NOT NULL,
  PRIMARY KEY (`STATIC_SMPP_TLV_TAG_ID`),
  UNIQUE KEY `DISPLAY_NAME_UNIQUE` (`DISPLAY_NAME`),
  UNIQUE KEY `EXAMPLE_TEXT_UNIQUE` (`EXAMPLE_TEXT`),
  UNIQUE KEY `TAG_ID_UNIQUE` (`PARAM_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;


====================================================================================================

INSERT INTO `static_smpp_tlv_tag` (`STATIC_SMPP_TLV_TAG_ID`, `PARAM_ID`, `DISPLAY_NAME`, `EXAMPLE_TEXT`) VALUES
	(1, 29, 'ADDITIONAL_STATUS_INFO_TEXT', 'label.example.additionalStatusInfoText'),
	(2, 4876, 'ALERT_ON_MESSAGE_DELIVERY', 'label.example.alertOnMessageDelivery'),
	(3, 897, 'CALLBACK_NUM', 'label.example.callbackNum'),
	(4, 771, 'CALLBACK_NUM_ATAG', 'label.example.callbackNumAtag'),
	(5, 770, 'CALLBACK_NUM_PRES_IND', 'label.example.callbackNumPresInd'),
	(6, 1061, 'DELIVERY_FAILURE_REASON', 'label.example.deliveryFailureReason'),
	(7, 523, 'DESTINATION_PORT', 'label.example.destinationPort'),
	(8, 5, 'DEST_ADDR_SUBUNIT', 'label.example.destAddrSubUnit'),
	(9, 7, 'DEST_BEARER_TYPE', 'label.example.destBearerType'),
	(10, 6, 'DEST_NETWORK_TYPE', 'label.example.destNetworkType'),
	(11, 515, 'DEST_SUBADDRESS', 'label.example.destSubaddress'),
	(12, 8, 'DEST_TELEMATICS_ID', 'label.example.destTelematicsId'),
	(13, 4609, 'DISPLAY_TIME', 'label.example.displayTime'),
	(14, 1056, 'DPF_RESULT', 'label.example.dpfResult'),
	(15, 4992, 'ITS_REPLY_TYPE', 'label.example.itsReplyType'),
	(16, 4995, 'ITS_SESSION_INFO', 'label.example.itsSessionInfo'),
	(17, 525, 'LANGUAGE_INDICATOR', 'label.example.languageIndicator'),
	(18, 1062, 'MORE_MESSAGES_TO_SEND', 'label.example.moreMessagesToSend'),
	(19, 1058, 'MS_AVAILABILITY_STATUS', 'label.example.msAvailabilityStatus'),
	(20, 48, 'MS_MSG_WAIT_FACILITIES', 'label.example.msMsgWaitFacilities'),
	(21, 4612, 'MS_VALIDITY', 'label.example.msValidity'),
	(22, 1059, 'NETWORK_ERROR_CODE', 'label.example.networkErrorCode'),
	(23, 772, 'NUMBER_OF_MESSAGES', 'label.example.numberOfMessages'),
	(24, 25, 'PAYLOAD_TYPE', 'label.example.payloadType'),
	(25, 513, 'PRIVACY_INDICATOR', 'label.example.privacyIndicator'),
	(26, 23, 'QOS_TIME_TO_LIVE', 'label.example.qosTimeToLive'),
	(27, 30, 'RECEIPTED_MESSAGE_ID', 'label.example.receiptedMessageId'),
	(28, 528, 'SC_INTERFACE_VERSION', 'label.example.scInterfaceVersion'),
	(29, 1057, 'SET_DPF', 'label.example.setDpf'),
	(30, 4611, 'SMS_SIGNAL', 'label.example.smsSignal'),
	(31, 13, 'SOURCE_ADDR_SUBUNIT', 'label.example.sourceAddrSubUnit'),
	(32, 15, 'SOURCE_BEARER_TYPE', 'label.example.sourceBearerType'),
	(33, 14, 'SOURCE_NETWORK_TYPE', 'label.example.sourceNetworkType'),
	(34, 522, 'SOURCE_PORT', 'label.example.sourcePort'),
	(35, 514, 'SOURCE_SUBADDRESS', 'label.example.sourceSubaddress'),
	(36, 16, 'SOURCE_TELEMATICS_ID', 'label.example.sourceTelematicsId'),
	(37, 516, 'USER_MESSAGE_REFERENCE', 'label.example.userMessageReference'),
	(38, 517, 'USER_RESPONSE_CODE', 'label.example.userResponseCode'),
	(39, 1281, 'USSD_SERVICE_OP', 'label.example.ussdServiceOp');
	
	
	========================================================================================================================================

ALTER TABLE `conf_mobile_address` 
ADD COLUMN `PARTNER_ID` BIGINT(20) NULL AFTER `GLOBAL_STOP_MOBILE_ADDRESS_ID`,
ADD COLUMN `system_defined` TINYINT NULL AFTER `PARTNER_ID`,
ADD INDEX `conf_mobile_address_partner_id_fk_idx` (`PARTNER_ID` ASC),
ADD CONSTRAINT `conf_mobile_address_partner_id_fk`
  FOREIGN KEY (`PARTNER_ID`)
  REFERENCES `partner` (`PARTNER_ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

===========================================================================================================================================
 
 /*migrate all the existing mobile address  rules to demo global partner id */
SET SQL_SAFE_UPDATES = 0;
update conf_mobile_address set partner_id=1;
SET SQL_SAFE_UPDATES = 1;



